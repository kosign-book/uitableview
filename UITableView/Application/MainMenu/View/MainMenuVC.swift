//
//  MainMenuVC.swift
//  UITableView
//
//  Created by Navy on 14/9/22.
//

import UIKit



class MainMenuVC: UIViewController {
    
    // MARK: - @IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Properties
    let mainMenuVM = MainMenuVM()
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        mainMenuVM.initCellData()
        super.viewDidLoad()
    }

}


// MARK: - UIViewController
extension MainMenuVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainMenuVM.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = self.mainMenuVM.cells[indexPath.row].value as! MenuInfo
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainMenuCell", for: indexPath) as! MainMenuCell
        cell.titleLabel.text = data.title
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let data = self.mainMenuVM.cells[indexPath.row].value as! MenuInfo
        
        
        switch data.rowType {
      
            
        case .ArrayModeWithTableView:
            let vc = UIStoryboard.init(name: "ContactListSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactListVC") as? ContactListVC
            self.navigationController?.pushViewController(vc!, animated: true)

        case .AddDeleteUpdateRow:
            let vc = UIStoryboard.init(name: "ContactListSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactListVC") as? ContactListVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        case .LoadDataAnimation:
            let vc = UIStoryboard.init(name: "LoadDataAnimationSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoadDataAnimationVC") as? LoadDataAnimationVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        case .CollectionInsideTable:
            let vc = UIStoryboard.init(name: "CollectionInsideTableSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "CollectionInsideTableVC") as? CollectionInsideTableVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        case .TableViewSection:
            let vc = UIStoryboard.init(name: "TableViewSectionSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "TableViewSectionVC") as? TableViewSectionVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        case .HeaderAndFooter:
            let vc = UIStoryboard.init(name: "HeaderAndFooterSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "HeaderAndFooterVC") as? HeaderAndFooterVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        case .ReusableXibFile:
            let vc = UIStoryboard.init(name: "ReusingXibFileSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReusingXibFileVC") as? ReusingXibFileVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        case .Pagination:
            let vc = UIStoryboard.init(name: "PaginationSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "PaginationVC") as? PaginationVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        case .TableViewCell:
            let vc = UIStoryboard.init(name: "TableViewCellSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "TableViewCellVC") as? TableViewCellVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
    }
    
    
}
