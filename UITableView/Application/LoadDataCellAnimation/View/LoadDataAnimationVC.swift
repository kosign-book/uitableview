//
//  LoadDataAnimationVC.swift
//  UITableView
//
//  Created by Navy on 15/9/22.
//

import UIKit
import Foundation


enum AnimtionType: String, CaseIterable {
    case Fade
    case MoveUpWithBounce
    case MoveUpWithFade
    case SlideIn
}

class LoadDataAnimationVC: UIViewController {

    // MARK: - @IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Variable
    var animationType = AnimtionType.Fade
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    
   
    
    
    
    // MARK: - IBAction
    @IBAction func animation1(_ sender: UIButton) {
        animationType = .Fade
        tableView.reloadData()
    }
    
    @IBAction func animation2(_ sender: UIButton) {
        animationType = .MoveUpWithBounce
        tableView.reloadData()
    }
    
    @IBAction func animation3(_ sender: UIButton) {
        animationType = .MoveUpWithFade
        tableView.reloadData()
    }
    
    @IBAction func animation4(_ sender: UIButton) {
        animationType = .SlideIn
        tableView.reloadData()
    }
    
    
    
    
    
    
    
    
    
    
    
    
}

// MARK: - UITableView
extension LoadDataAnimationVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        switch animationType {
        case .Fade :
            cell.alpha = 0
            UIView.animate(withDuration: 0.6,delay: 0.05 * Double(indexPath.row),animations: {
                cell.alpha = 1
            })
            
        case .MoveUpWithBounce:
            let animation = AnimationFactory.makeMoveUpWithFade(rowHeight: cell.frame.height, duration: 0.5, delayFactor: 0.05)
            let animator = Animator(animation: animation)
            animator.animate(cell: cell, at: indexPath, in: tableView)
            
        case .MoveUpWithFade:
            let animation = AnimationFactory.makeMoveUpWithBounce(rowHeight: cell.frame.height, duration: 1.0, delayFactor: 0.05)
            let animator = Animator(animation: animation)
            animator.animate(cell: cell, at: indexPath, in: tableView)
            
        case .SlideIn:
            let animation = AnimationFactory.makeSlideIn(duration: 0.5, delayFactor: 0.05)
            let animator = Animator(animation: animation)
            animator.animate(cell: cell, at: indexPath, in: tableView)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoadDataAnimationCell", for: indexPath) as! LoadDataAnimationCell
        return cell
    }
    
}






