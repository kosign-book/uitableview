//
//  AwakeFromNibVC.swift
//  UITableView
//
//  Created by Navy on 28/9/22.
//

import UIKit

class AwakeFromNibVC: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}


extension AwakeFromNibVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AwakeFromNibCell", for: indexPath) as! AwakeFromNibCell
        return cell
    }
    
    
}
