//
//  TableViewCellMenuCell.swift
//  UITableView
//
//  Created by Navy on 28/9/22.
//

import UIKit

class TableViewCellMenuCell: UITableViewCell {
    @IBOutlet weak var titleLabel       : UILabel!
    @IBOutlet weak var containerView    : UIView!
}
