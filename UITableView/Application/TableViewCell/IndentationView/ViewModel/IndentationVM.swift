//
//  IndentationVM.swift
//  UITableView
//
//  Created by Navy on 3/10/22.
//

import Foundation


class IndentationVM {
    
    // variables
    var cells : [IndentationModel<Any>] = []

    // functions
    func initData() {
        
        cells = [] // Must clear array before append new values, to prevent data redundancy
        
        // profile cell (mobile owner info)
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "A", name: ["Andrew", "Alexa", "Anna", "Adagio","Ava","Amelia","Abigail", "Alexander", ])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "B", name: ["Ben", "Batista", "BAILEY", "BAKER", "BALTAZAR", "BARON", "BARRETT"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "C", name: ["Caterina","CADE","CADEN","CALVIN"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "D", name: ["Donnie","DESMOND","DEV","DIEGO","DILAN","DOMINIK","DOUGLAS"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "E", name: ["Elizaberth","EDUARDO","EESA","ELEAZAR","ELIAH","ELIAN"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "F", name: ["Frank","FABIAN","FERNANDO","FIDEL","FINNEGAN"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "G", name: ["Goerge","GENARO","GEOVANNI","GERMAN","GERSON","GIANLUCA"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "H", name: ["Hernandes", "HAWK", "HARRIS"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "I", name: ["Idris"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "J", name: ["Jackie"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "K", name: ["Kevin"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "L", name: ["Leo"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "M", name: ["Manne","Manny Pageoout"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "N", name: ["Nanny", "Nicky"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "S", name: ["Sanda","Solar","Sishi", "Susu"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "T", name: ["Tronsmart", "Tomb", "Tessa"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "U", name: ["Usaka"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "W", name: ["Wayn", "Walker"])))
        cells.append(IndentationModel<Any>(value: IndentationInfo(header: "Z", name: ["Zooooo"])))

    }
  

}
