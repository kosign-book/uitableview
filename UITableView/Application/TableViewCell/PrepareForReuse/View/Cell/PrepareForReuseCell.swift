//
//  PrepareForReuseCell.swift
//  UITableView
//
//  Created by Navy on 29/9/22.
//

import UIKit

class PrepareForReuseCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel    : UILabel!
    @IBOutlet weak var careerLabel  : UILabel!
    

    // prepareForReuse() is used to clear data before it's being reused at other cell to prevent displaying data redundancy
    override func prepareForReuse() {
        nameLabel.text?.removeAll()
        careerLabel.text?.removeAll()
        profileImage.image = nil
    }
    
    func configCell(name: String, career: String, image: String) {
        profileImage.image = UIImage(named: image)
        nameLabel.text = name
        careerLabel.text = career
    }

}
