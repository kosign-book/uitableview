//
//  MessengerVC.swift
//  UITableView
//
//  Created by Navy on 29/9/22.
//

import UIKit

class MessengerVC: UIViewController {

    // MARK: - @IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Variables
    let messagerVM = MessengerVM()
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.messagerVM.initData()
    }
    
}

// MARK: - UITableView
extension MessengerVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        messagerVM.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = messagerVM.cells[indexPath.row].value as! MessengerInfo
        let cell = tableView.dequeueReusableCell(withIdentifier: "SwipeForOptionCell", for: indexPath) as! SwipeForOptionCell
        cell.configCell(profile: data.profileImg, name: data.name, message: data.message, unreadCount: data.newMsgCount)
        return cell
    }
    
    // SWIPE TO LEFT
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        // DELETE BUTTON
        let deleteButton = UIContextualAction(style: .destructive, title: "Delete") { [weak self] (action, view, completionHandler) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                // first - delete obj from data record
                self?.messagerVM.cells.remove(at: indexPath.row)
                
                // then - delete row from tableView
                let index = IndexPath(row: indexPath.row , section: indexPath.section)
                self?.tableView.deleteRows(at: [index], with: .fade)
                
            }
            completionHandler(true)
        }
        deleteButton.backgroundColor = .systemRed
        
        
        // ARCHIVE BUTTON
        let archiveButton = UIContextualAction(style: .normal, title: "Archive") { [weak self] (action, view, completionHandler) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                // first - delete obj from data record
                self?.messagerVM.cells.remove(at: indexPath.row)
                
                // then - delete row from tableView
                let index = IndexPath(row: indexPath.row , section: indexPath.section)
                self?.tableView.deleteRows(at: [index], with: .fade)
            }
            completionHandler(true)
        }
        archiveButton.backgroundColor = .gray


        // MARK AS READ BUTTON
        let markAsReadButton = UIContextualAction(style: .normal, title: "Mark as read") { [weak self] (action, view, completionHandler) in
            
            // update data in ViewModel
            self?.messagerVM.markAsRead(index: indexPath.row, newMsgCount: 0)
            
            // reload specific row for status change
            let editingIndex = IndexPath(row: indexPath.row, section: indexPath.section)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                self?.tableView.reloadRows(at: [editingIndex], with: .none)
            }
            completionHandler(true)
        }
        markAsReadButton.backgroundColor = .systemOrange
        
        
        // return all 3 button
        let configuration = UISwipeActionsConfiguration(actions: [deleteButton, archiveButton, markAsReadButton])
        return configuration
    }
    
    
    // SWIPE TO RIGHT
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let closeAction = UIContextualAction(style: .normal, title:  "Close", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("OK, marked as Closed")
            success(true)
        })
        closeAction.image = UIImage(named: "tick")
        closeAction.backgroundColor = .purple
        return UISwipeActionsConfiguration(actions: [closeAction])
    }
    
}
