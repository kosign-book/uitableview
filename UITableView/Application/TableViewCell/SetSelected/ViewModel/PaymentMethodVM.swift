//
//  PaymentMethodVM.swift
//  UITableView
//
//  Created by Navy on 28/9/22.
//

import Foundation

class PaymentMethodVM {
    // variables
    var cells : [PaymentMethodModel<Any>] = []
    
    // functions
    func initCellData() {
        
        cells = [] // Must clear array before append new values, to prevent data redundancy
        cells.append(PaymentMethodModel<Any>(value: PaymentMethodInfo(title: "Kidney", icon: "payment_6")))
        cells.append(PaymentMethodModel<Any>(value: PaymentMethodInfo(title: "Visa Card", icon: "payment_1")))
        cells.append(PaymentMethodModel<Any>(value: PaymentMethodInfo(title: "Master Card", icon: "payment_2")))
        cells.append(PaymentMethodModel<Any>(value: PaymentMethodInfo(title: "American Express", icon: "payment_3")))
        cells.append(PaymentMethodModel<Any>(value: PaymentMethodInfo(title: "PayPal", icon: "payment_4")))


    }
    
}
