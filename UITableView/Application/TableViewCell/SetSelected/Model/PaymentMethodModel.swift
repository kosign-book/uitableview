//
//  SetSelectedModel.swift
//  UITableView
//
//  Created by Navy on 28/9/22.
//

import Foundation

// big array model to store all menu
struct PaymentMethodModel <T> {
    var value        : T?
}

// properties of each menu
struct PaymentMethodInfo {
    var title   : String
    var icon    : String
}
