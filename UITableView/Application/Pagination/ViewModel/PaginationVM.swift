//
//  PaginationVM.swift
//  UITableView
//
//  Created by Navy on 27/9/22.
//

import Foundation

class PaginationVM {
    // variables
    var cells : [PaginationModel<Any>] = []
    
    // functions
    func initCellData() {
        
        cells = [] // Must clear array before append new values, to prevent data redundancy
        
        cells.append(PaginationModel<Any>(value: PaginationInfo(title: "Default existed data", bgColor: .orange)))
        cells.append(PaginationModel<Any>(value: PaginationInfo(title: "Default existed data", bgColor: .orange)))
        cells.append(PaginationModel<Any>(value: PaginationInfo(title: "Default existed data", bgColor: .orange)))
        cells.append(PaginationModel<Any>(value: PaginationInfo(title: "Default existed data", bgColor: .orange)))
        cells.append(PaginationModel<Any>(value: PaginationInfo(title: "Default existed data", bgColor: .orange)))
        cells.append(PaginationModel<Any>(value: PaginationInfo(title: "Default existed data", bgColor: .orange)))
        cells.append(PaginationModel<Any>(value: PaginationInfo(title: "Default existed data", bgColor: .orange)))
        cells.append(PaginationModel<Any>(value: PaginationInfo(title: "Default existed data", bgColor: .orange)))
        cells.append(PaginationModel<Any>(value: PaginationInfo(title: "Default existed data", bgColor: .orange)))
        cells.append(PaginationModel<Any>(value: PaginationInfo(title: "Default existed data", bgColor: .orange)))
        cells.append(PaginationModel<Any>(value: PaginationInfo(title: "Default existed data", bgColor: .orange)))
        cells.append(PaginationModel<Any>(value: PaginationInfo(title: "Default existed data", bgColor: .orange)))
        cells.append(PaginationModel<Any>(value: PaginationInfo(title: "Default existed data", bgColor: .orange)))
        cells.append(PaginationModel<Any>(value: PaginationInfo(title: "Default existed data", bgColor: .orange)))

    }
}
