//
//  ContactModel.swift
//  UITableView
//
//  Created by Navy on 24/8/22.
//

import Foundation


enum RowType: String, CaseIterable {
    case Profile
    case Contact
}

// big array model to store all contacts
struct ContactModel <T> {
    var value        : T?
}

// properties of each contact
struct ContactInfo {
    var phone_number : String
    var contact_name : String
    var profile_img  : String
    var rowType      : RowType
}


