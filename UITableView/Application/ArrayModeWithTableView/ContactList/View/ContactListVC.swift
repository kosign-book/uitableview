//
//  ViewController.swift
//  UITableView
//
//  Created by Navy on 24/8/22.
//

import UIKit

class ContactListVC: UIViewController {

    // MARK: - @IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Variables
    var contactVM = ContactVM()
    
    
    // MARK: - LifeCyle
    override func viewDidLoad() {
        super.viewDidLoad()
        contactVM.initCellData()
    }
    
    // MARK: - @IBAction
    @IBAction func addBarButtonItemDidTap(_ sender: Any) {
        
        let vc = self.callCommonPopup(withStorybordName: "CustomPopupSB", identifier: "CustomPopupVC") as! CustomPopupVC
        
        vc.saveCompletion = { name, phoneNumber in
            
            
            // add new obj to data record
            self.contactVM.addNewContact(name: name, phoneNumber: phoneNumber)
            
            // add new row into tableView
//            let lastIndex = self.contactVM.cells.count - 1
            self.tableView.beginUpdates()
            
            
            
            
            self.tableView.insertRows(at: [IndexPath.init(row: self.contactVM.cells.count-1 , section: 0)], with: .automatic)
            
            
            
            
            self.tableView.endUpdates()
        }
        self.present(vc, animated: true, completion: nil)
    }
    
}

// MARK: - UITableView
extension ContactListVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactVM.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // get data from view model
        let data = self.contactVM.cells[indexPath.row].value as! ContactInfo
        
        switch data.rowType {
        case .Profile:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
            cell.configCell(data: data)
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactCell
            
            // add action to each button in cell when pressed
            cell.deleteButton.addTarget(self, action: #selector(deleteContact(sender:)), for: .touchUpInside)
            cell.editButton.addTarget(self, action: #selector(editContact(sender:)), for: .touchUpInside)
            cell.callButton.addTarget(self, action: #selector(callContact(sender:)), for: .touchUpInside)
            // set data for cell
            cell.configCell(data: data)
            
            return cell
        }
    }
}



extension ContactListVC {
    
    // MARK: - Objective-C
    // Delete Contact
    @objc private func deleteContact(sender: UIButton) {
        // get indexPath of selected row
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
    
        let data = contactVM.cells[indexPath?.row ?? 0].value as! ContactInfo
        let contactName = data.contact_name
        
        // title & subtitle of alert
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to delete \(contactName)?", preferredStyle: UIAlertController.Style.alert)

        // OK button action
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            
            
            // first - delete obj from data record
            self.contactVM.cells.remove(at: indexPath?.row ?? self.contactVM.cells.count - 1)
            
            
            // then - delete row from tableView
            let index = IndexPath(row: indexPath?.row ?? 0, section: indexPath?.section ?? 0)
            self.tableView.deleteRows(at: [index], with: .fade)
            
            
            
        }))

        // Cancel button action
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            // write cancel action here
        }))

        present(alert, animated: true, completion: nil)
    }
    
    // Edit Contact
    @objc private func editContact(sender: UIButton) {
        // get index of selected row for editing
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        // retrieve existed data to display on popup
        let data = contactVM.cells[indexPath?.row ?? 0].value as! ContactInfo
        
        let vc = self.callCommonPopup(withStorybordName: "CustomPopupSB", identifier: "CustomPopupVC") as! CustomPopupVC
        vc.name         = data.contact_name
        vc.phoneNumber  = data.phone_number
        vc.profile      = data.profile_img
        vc.titleString  = data.contact_name
        vc.isEdit       = true
    
        // done editing completion
        vc.saveCompletion = { name, phoneNumber in
            
            
            // update obj in data record
            self.contactVM.editContact(name: name, phoneNumber: phoneNumber, index: indexPath?.row ?? 0)
            
            //   self.tableView.reloadData()
            
//             reload only edited row
            let editingIndex = IndexPath(row: indexPath?.row ?? 0, section: indexPath?.section ?? 0)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                self.tableView.reloadRows(at: [editingIndex], with: .none)
            }
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    
    // Call Contact
    @objc private func callContact(sender: UIButton) {
        
        // get index of selected row
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        // get selected contact's number to call
        let data = contactVM.cells[indexPath?.row ?? 0].value as! ContactInfo
        let phoneNumber = data.phone_number.replacingOccurrences(of: " ", with: "")
        
        // open phone call app
        if let url = URL(string: "tel://\(phoneNumber)") {
            UIApplication.shared.openURL(url)
        }
    }
}

// call custom popup
extension UIViewController {
    func callCommonPopup(withStorybordName storyboard: String, identifier: String) -> UIViewController {
        let vc = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: identifier)
        vc.modalPresentationStyle                       = .overFullScreen
        vc.modalTransitionStyle                         = .crossDissolve
        vc.providesPresentationContextTransitionStyle   = true
        vc.definesPresentationContext                   = true
        return vc
    }
}
