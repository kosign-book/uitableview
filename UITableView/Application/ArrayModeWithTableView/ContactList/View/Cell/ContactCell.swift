//
//  ContactCell.swift
//  UITableView
//
//  Created by Navy on 24/8/22.
//

import UIKit

class ContactCell: UITableViewCell {
    
    @IBOutlet weak var contactNameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profileImageView.layer.cornerRadius = 20
        callButton.layer.cornerRadius = 4
        editButton.layer.cornerRadius = 4
        deleteButton.layer.cornerRadius = 4
        
        deleteButton.layer.borderWidth = 1
        deleteButton.layer.borderColor = UIColor.init(hexString: "04537D").cgColor
        
        editButton.layer.borderWidth = 1
        editButton.layer.borderColor = UIColor.init(hexString: "04537D").cgColor
        
        callButton.layer.borderWidth = 1
        callButton.layer.borderColor = UIColor.init(hexString: "04537D").cgColor
    }
    
    func configCell(data: ContactInfo) {
        self.contactNameLabel.text = data.contact_name
        self.phoneNumberLabel.text = data.phone_number
        self.profileImageView.image = UIImage(named: data.profile_img)
    }
}
